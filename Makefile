.PHONY: manual
manual:
	(cd docs; python3 site.py)
	cp docs/*.png docs/output/
	cp docs/*.svg docs/output/

.PHONY: deps
deps:
	pip3 install -r requirements.txt

.PHONY: clean
clean:
	rm -rf dist
	rm -rf build
	rm -rf warpseq.egg-info

.PHONY: wheel
wheel: clean
	python3 setup.py sdist bdist_wheel

.PHONY: pypi_upload
pypi_upload: wheel
	# project owners only, after bumping version
	python3 setup.py pypi_upload

.PHONY: pyflakes
pyflakes:
	pyflakes warpseq/

.PHONY: test
test:
	PYTHONPATH=. python3 tests/assembly.py

.PHONY: pytest
pytest:
	PYTHONPATH=. python3 tests/*.py

.PHONY: pycodestyle
pycodestyle:
	pycodestyle -r warpseq/

.PHONY: pep8
pep8: pycodestyle
