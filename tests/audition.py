# --------------------------------------------------------------
# Warp API Demo
# (C) Michael DeHaan <michael@michaeldehaan.net>, 2020
# --------------------------------------------------------------
#
# this demos hows how instruments can pull notes selectively
# from a silent guide track, which can be useful to rapidly
# construct large movements without having to sequence notes
# for every single instrument.
#
# Basically one instrument can "listen" to what is playing on another track, copy the note,
# and then use it to build chords, transpose up or down, or apply an arpeggiator.

from warpseq.api import demo
from warpseq.api.public import Api as WarpApi
from warpseq.model.slot import Slot

from warpseq.api.interfaces.patterns import INTERNAL_AUDITION_SCENE

# ======================================================================================================================
# setup API and song

api = WarpApi()
api.song.edit(tempo=120)

# ======================================================================================================================
# setup instruments

DEVICE = demo.suggest_device(api, 'IAC Driver IAC Bus 1')

api.instruments.add('guide', device=DEVICE, channel=4, min_octave=0, base_octave=4, max_octave=10, muted=True)
api.instruments.add('lead_inst', device=DEVICE, channel=1, min_octave=0, base_octave=4, max_octave=10, muted=False)
api.instruments.add('bass_inst', device=DEVICE, channel=2, min_octave=0, base_octave=4, max_octave=10,muted=False)

# ======================================================================================================================
# setup tracks

api.tracks.add(name='guide', instrument='guide')
api.tracks.add(name='lead', instrument='lead_inst')
api.tracks.add(name='bass', instrument='bass_inst')

# ======================================================================================================================
# setup scales

api.scales.add(name='C-major', note='C', octave=0, scale_type='major')

# ======================================================================================================================
# setup patterns

api.patterns.add(name='a', audition_with='lead_inst', slots=[
    Slot(degree=1),
    Slot(degree=3),
    Slot(degree=4),
    Slot(degree=4),
    Slot(degree=6),
    Slot(degree=7),
])

# ======================================================================================================================
# setup transforms

api.transforms.add(name='t1', audition_instrument='lead_inst', audition_pattern='a', slots=[
    Slot(octave_shift=1),
    Slot(octave_shift=2),
    Slot(octave_shift=3),
])

# ======================================================================================================================
# go!


#api.save_as("/tmp/test.json")
#api.player.loop('scene_1')

#api.player.audition_pattern('a')
api.player.audition_transform('t1')

api.player.loop()

#api.player.loop(INTERNAL_AUDITION_SCENE)

#instruments = api2.instruments.list()
#tracks = api2.tracks.list()
#scenes = api2.scenes.list()
#clips = api2.clips.list()
#patterns = api2.patterns.list()
#transforms = api2.transforms.list()

#print("instruments=%s" % instruments)
#print("tracks=%s" % tracks)
#print("scenes=%s" % scenes)
#print("clips=%s" % clips)
#print("patterns=%s" % patterns)
#print("transforms=%s" % transforms)

#api.save_as("/tmp/test2.json")

#api2.player.loop('scene_1')